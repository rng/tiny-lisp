#!/usr/bin/env python
import argparse
import readline
import atexit
import os

if os.path.exists(".replhist"):
    readline.read_history_file(".replhist")
atexit.register(readline.write_history_file, ".replhist")


def tokenise(raw):
  toks = []
  p = 0
  while p < len(raw):
    while p < len(raw) and raw[p] in " \t\r\n": p += 1
    if p >= len(raw):
      break
    if raw[p] == ";":
      while p < len(raw) and raw[p] != "\n": p += 1
      continue
    elif raw[p] in "()":
      toks.append(raw[p]); p += 1
    elif raw[p].isdigit():
      v = ""
      while raw[p].isdigit():
        v += raw[p]; p += 1
      toks.append(int(v))
    elif raw[p].isalpha() or raw[p].isdigit() or raw[p] in "+-*/_=!%^><?":
      v = ""
      while p < len (raw) and (raw[p].isalpha() or raw[p].isdigit() or raw[p] in "+-*/_=!%^><?"):
        v += raw[p]; p += 1
      toks.append(v)
    else:
      raise Exception("unhandled character: %s" % raw[p])
  return toks


def parse_expr(toks):
  if toks[0] == "(":
    r = []
    toks.pop(0)
    while toks[0] != ")":
      r.append(parse_expr(toks))
    toks.pop(0)
    return tuple(r)
  elif isinstance(toks[0], (int, str)):
    return toks.pop(0)
  else:
    raise


def parse(toks):
  r = []
  while len(toks):
    r.append(parse_expr(toks))
  return r


def find_var(expr, globs, funcargs, env):
  if expr in env:
    return ("e", env.index(expr))
  for i, a in enumerate(funcargs):
    if expr in a:
      if i == 0:
        return ("a", a.index(expr))
      env.append(expr)
      return ("e", len(env)-1)
  if expr in globs:
    return ("g", globs.index(expr))
  raise Exception("unknown variable '%s'" % expr)


def comp_store(cg, rs, name, globs, funcargs, env):
  typ, idx = find_var(name, globs, funcargs, env)
  if typ == "a":   return cg.gen_st(rs, "fp", -(4+idx))
  elif typ == "e": return cg.gen_st(rs, "re", idx)
  elif typ == "g": return cg.gen_st(rs, "gl", idx)


def comp_load(cg, name, globs, funcargs, env):
  typ, idx = find_var(name, globs, funcargs, env)
  if typ == "a":   return cg.gen_ld("fp", -(4+idx))
  elif typ == "e": return cg.gen_ld("re", idx)
  elif typ == "g": return cg.gen_ld("gl", idx)


CLOS_ARGC, CLOS_START, CLOS_ENV, CLOS_SIZE = range(4)
TYPE_CLOS = 1


def comp_expr(cg, expr, globs, funcargs, env, tail=False):
  if isinstance(expr, int):
    return cg.gen_int(expr)
  elif isinstance(expr, str):
    return comp_load(cg, expr, globs, funcargs, env)
  elif isinstance(expr, tuple):
    if len(expr) == 0:
      return cg.gen_int(0)
    else:
      fn, args = expr[0], expr[1:]
      if fn == "def":
        assert len(args) == 2
        if args[0] not in globs: globs.append(args[0])
        rs = comp_expr(cg, args[1], globs, funcargs, env, False)
        return comp_store(cg, rs, args[0], globs, funcargs, env)
      elif fn == "set":
        assert len(args) == 2
        rs = comp_expr(cg, args[1], globs, funcargs, env, False)
        return comp_store(cg, rs, args[0], globs, funcargs, env)
      elif fn == "lambda":
        assert len(args) == 2
        j = cg.gen_jmp()
        env = []
        ctmp = cg.t; cg.t = -1
        # generate function body
        cg.gen_push("fp")
        cg.gen_mov("fp", "sp")
        rs = comp_expr(cg, args[1], globs, [args[0]]+funcargs, env, True)
        cg.gen_mov("sp", "fp")
        cg.gen_pop("fp")
        cg.gen_ret(rs)
        codestart = cg.funcaddr(j)
        codelen = cg.codeaddr(len(cg.code)) - codestart
        cg.patch(j)
        cg.t = ctmp
        # build code block
        rf = cg.gen_alloc(codelen)
        cg.gen_memcpy(rf, codestart, codelen)
        cg.gen_push(rf)
        # build environment (nil if no closed variables)
        re = cg.gen_int(0) if len(env) == 0 else cg.gen_alloc(len(env)*4)
        for i, e in enumerate(env):
          rv = comp_load(cg, e, globs, funcargs, [])
          cg.gen_st(rv, re, i)
          cg.unused(rv)
        cg.gen_push(re)
        # lambda
        rl = cg.gen_alloc(CLOS_SIZE*4)
        # store lambda arg count
        ri = cg.gen_int(len(args[0]))
        cg.gen_st(ri, rl, CLOS_ARGC)
        cg.unused(ri)
        # store lambda environment
        re = cg._push(); cg.gen_pop(re)
        cg.gen_st(re, rl, CLOS_ENV)
        cg.unused(re)
        # store lambda start address
        rf = cg._push(); cg.gen_pop(rf)
        cg.gen_st(rf, rl, CLOS_START)
        cg.unused(rf)
        return rl
      elif fn == "progn":
        for i, exp in enumerate(args):
          tail = (i == len(args)-1)
          ra = comp_expr(cg, exp, globs, funcargs, env, tail)
          if not tail:
            cg.unused(ra)
        return ra
      elif fn == "if":
        assert 2 <= len(args) <= 3
        rp = comp_expr(cg, args[0], globs, funcargs, env, False)
        cg.gen_cmp(rp)
        jf = cg.gen_jmp(cond=True)
        ra = comp_expr(cg, args[1], globs, funcargs, env, tail)
        jd = cg.gen_jmp()
        cg.patch(jf)
        if len(args) == 3:
            rb = comp_expr(cg, args[2], globs, funcargs, env, tail)
            cg.gen_mov(ra, rb)
        cg.patch(jd)
        return ra
      elif fn == "print":
        rs = comp_expr(cg, args[0], globs, funcargs, env, False)
        return cg.gen_prim(fn, [rs])
      elif fn in [">", "<", "-", "+", "*", "/", "="]:
        if len(args) != 2:
          raise Exception("Invalid number of arguments to primitive")
        rs0 = comp_expr(cg, args[0], globs, funcargs, env, False)
        cg.gen_push(rs0)
        rs1 = comp_expr(cg, args[1], globs, funcargs, env, False)
        rs0 = cg._push()
        cg.gen_pop(rs0)
        return cg.gen_prim(fn, [rs1, rs0])
      else:
        for a in reversed(args):
          ra = comp_expr(cg, a, globs, funcargs, env, False)
          cg.gen_push(ra)
        if tail: # tail call
          for i in range(len(args)):
            ra = cg._push()
            cg.gen_pop(ra)
            cg.gen_st(ra, "fp", -(4+i))
            cg.unused(ra)
          rf = comp_expr(cg, fn, globs, funcargs, env, False)
          cg.gen_chkt(rf, TYPE_CLOS)
          cg.gen_ld(rf, CLOS_ENV, "re")
          ra = cg.gen_ld(rf, CLOS_ARGC)
          cg.gen_chka(ra, len(args))
          cg.gen_ld(rf, CLOS_START, rf)
          cg.gen_call(rf, len(args), tail=True)
        else: # non tail call
          rf = comp_expr(cg, fn, globs, funcargs, env, False)
          cg.gen_chkt(rf, TYPE_CLOS)
          cg.gen_push("re")
          cg.gen_ld(rf, CLOS_ENV, "re")
          ra = cg.gen_ld(rf, CLOS_ARGC)
          cg.gen_chka(ra, len(args))
          cg.gen_ld(rf, CLOS_START, rf)
          cg.gen_call(rf, len(args), tail=False)
          cg.gen_pop("re")
          cg.gen_drop(len(args))
        return cg.gen_mov(cg._push(), "rr")
        return "rr"
  else:
    raise


def comp(cg, exprs, globs):
  for expr in exprs:
    comp_expr(cg, expr, globs, [], [])
    cg.t = -1
  cg.gen_halt()


def comp_run(cg, backend, globs, source):
  exprs = parse(tokenise(source))
  cg.reset()
  comp(cg, exprs, globs)
  backend.disasm(cg)
  return backend.run(cg)


def rep(cg, backend, globs):
  raw = raw_input(">> ")
  print comp_run(cg, backend, globs, raw)


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('files', metavar='SOURCE', nargs='+')
  parser.add_argument("-b", "--backend", default="vm", choices=["x86", "vm"])
  parser.add_argument("-r", "--repl", action="store_true")
  args = parser.parse_args()

  if args.backend == "vm":
    import backend_vm as backend
  elif args.backend == "x86":
    import backend_x86 as backend

  globs = []
  cg = backend.Cg()
  comp_run(cg, backend, globs, open(args.files[0]).read())
  if args.repl:
    while 1:
      try:
        rep(cg, backend, globs)
      except Exception as e:
        print "Error:", e


main()
