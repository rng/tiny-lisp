GLOB_START, GLOB_END = 1900, 2000
HEAP_START, HEAP_END = 2000, 4000
STACK_START, STACK_END = 4000, 4096


class Cg(object):
  def __init__(self):
    self.reset()
    self.membuf = [0] * 4096
    self.nextalloc = HEAP_START

  def reset(self):
    self.code = []
    self.t = -1

  def _push(self):
    self.t+=1
    assert 2 > self.t >= 0
    return "r%s" % ("ab"[self.t])

  def _pop(self, *regs):
    for r in regs:
      assert isinstance(r, str)
      if r not in ["rr", "re", "sp", "fp", "gl"]:
        self.t -= 1

  def unused(self, reg):
    self._pop(reg)

  def _i(self, ret, inst):
    self.code.append(inst)
    return ret

  def gen_mov(self, rd, rs):
    self._pop(rs)
    return self._i(rd, ("mov", rd, rs))

  def gen_cmp(self, rs):
    self._pop(rs)
    return self._i(None, ("cmp", rs))

  def gen_ld(self, rs, v, rd=None):
    if rd is None: rd = self._push()
    return self._i(rd, ("ld", rd, rs, v))

  def gen_st(self, rs, rb, v):
    return self._i(rs, ("st", rs, rb, v))

  def gen_int(self, v):
    rd = self._push()
    return self._i(rd, ("int", rd, v))

  def gen_prim(self, fn, rs):
    self._pop(*rs)
    rd = self._push()
    return self._i(rd, ("prim", fn, rd, rs))

  def gen_ret(self, rs):
    self._pop(rs)
    return self._i(rs, ("ret", rs))

  def gen_alloc(self, n):
    rd = self._push()
    return self._i(rd, ("new", rd, n))
  
  def gen_push(self, rs):
    self._pop(rs)
    return self._i(rs, ("push", rs))

  def gen_pop(self, rd):
    self.code.append(("pop", rd))

  def gen_chka(self, rs, n):
    self._pop(rs)
    self.code.append(("chka", rs, n))

  def gen_chkt(self, rs, typ):
    self.code.append(("chkt", rs, typ))

  def gen_call(self, rf, n, tail):
    self._pop(rf)
    return self._i("rr", ("calt" if tail else "call", rf, n))

  def gen_memcpy(self, rd, src, size):
    return self._i(None, ("mcpy", rd, src, size))

  def gen_def(self, name):  self.code.append(("def", name))
  def gen_drop(self, n):    self.code.append(("drop", n))

  def gen_jmp(self, cond=False, addr=-1):
    self.code.append(("jmpf" if cond else "jmp", addr))
    return len(self.code)-1

  def gen_halt(self):
    self.code.append(("halt",))

  def patch(self, jumpaddr):
    assert ((self.code[jumpaddr] == ("jmp", -1)) or
            (self.code[jumpaddr] == ("jmpf", -1)))
    self.code[jumpaddr] = (self.code[jumpaddr][0], len(self.code))

  def codeaddr(self, ofs):
    return ofs

  def funcaddr(self, addr):
    return addr+1


def disasm(cg):
  for i, c in enumerate(cg.code):
    op, args = c[0], c[1:]
    print "%3d: %4s %s" % (i, op, " ".join(map(str, args)))
    if op == "ret":
      print


class Ptr(int):
  pass


def run(cg):
  regs = {"pc": 0, "re": 0, "rx": 0, "sp": STACK_START, "fp": 0, "gl": GLOB_START}
  for i, ins in enumerate(cg.code):
    cg.membuf[i] = ins
  assert GLOB_START >= len(cg.code)

  def throw(msg):
    handler = regs["rx"]
    if regs["rx"] == 0:
      raise Exception("no exception handler installed (%s)" % msg)
    regs["pc"] = cg.membuf[handler+1]
    regs["sp"] = cg.membuf[handler+2]
    regs["fp"] = cg.membuf[handler+3]
    regs["re"] = cg.membuf[handler+4]
    regs["rx"] = cg.membuf[handler+5]

  def psh(v):
    if (regs["sp"] >= STACK_END): throw("stack overflow")
    cg.membuf[regs["sp"]] = v
    regs["sp"] += 1

  def pop():
    regs["sp"] -= 1
    if (regs["sp"] < STACK_START): throw("stack underflow")
    return cg.membuf[regs["sp"]]

  def alloc(sz):
    r = cg.nextalloc
    cg.nextalloc += sz
    assert cg.nextalloc < HEAP_END
    return Ptr(r)

  while 1:
    #print "%3d: %4s %-10s |" % (regs["pc"], cg.membuf[regs["pc"]][0], 
    #                            ", ".join(map(str,cg.membuf[regs["pc"]][1:]))),
    #                            " ".join("%s:%s" % kv for kv in sorted(regs.items()))
    op, args = cg.membuf[regs["pc"]][0], cg.membuf[regs["pc"]][1:]
    regs["pc"] += 1
    if op == "halt":   break
    elif op == "jmp":  regs["pc"] = args[0]
    elif op == "jmpf":
      if regs["rc"]:
        regs["pc"] = args[0]
    elif op == "new":
      regs[args[0]] = alloc(args[1])
    elif op == "cmp":  regs["rc"] = regs[args[0]] == False
    elif op == "int":  regs[args[0]] = args[1]
    elif op == "mov":  regs[args[0]] = regs[args[1]]
    elif op == "pop":  regs[args[0]] = pop()
    elif op == "push": psh(regs[args[0]])
    elif op == "ld":   regs[args[0]] = cg.membuf[regs[args[1]] + args[2]]
    elif op == "st":   cg.membuf[regs[args[1]] + args[2]] = regs[args[0]]
    elif op == "chka":
      if regs[args[0]] != args[1]:
        throw("Incorrect number of args: expected %r, got %r" % (
          args[1], regs[args[0]]))
    elif op == "chkt":
      p = regs[args[0]]
      if args[1] == 1 and not isinstance(p, Ptr):
        throw("Invalid type")
    elif op == "call":
      fstart = regs[args[0]]
      psh(regs["pc"])
      regs["pc"] = fstart
    elif op == "calt":
      fstart = regs[args[0]]
      regs["pc"] = fstart+2
    elif op == "ret":
      regs['rr'] = regs[args[0]]
      regs["pc"] = pop()
    elif op == "mcpy":
      for i in range(args[2]):
        cg.membuf[regs[args[0]] + i] = cg.membuf[args[1] + i]
    elif op == "prim":
      prim, rd, rs = args
      if   prim == "+":     regs[rd] = regs[rs[1]] + regs[rs[0]]
      elif prim == "-":     regs[rd] = regs[rs[1]] - regs[rs[0]]
      elif prim == "*":     regs[rd] = regs[rs[1]] * regs[rs[0]]
      elif prim == "/":     regs[rd] = regs[rs[1]] / regs[rs[0]]
      elif prim == ">":     regs[rd] = int(regs[rs[1]] > regs[rs[0]])
      elif prim == "<":     regs[rd] = int(regs[rs[1]] < regs[rs[0]])
      elif prim == "=":     regs[rd] = int(regs[rs[1]] == regs[rs[0]])
      elif prim == "print": print ">", regs[rs[0]]
      else:
        throw("unhandled primitive %s" % prim)
    elif op == "drop":
      for _ in range(args[0]): pop()
    else:
      throw("unhandled instruction %s %s" % (op, args))
  return regs['ra']
