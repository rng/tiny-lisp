wc:
	find . -name "*.py" | xargs wc -l | sort -n

clean:
	find . -name "*.pyc" -delete

