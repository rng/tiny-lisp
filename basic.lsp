(def add2 (lambda (n) (+ n 2)))

(print (add2 3))
(print (+ 42 2))
(print (- 10 6))
(print (- (* (+ 2 (* 9 3)) 2) 16))
(print (> 3 4))
(if (> 3 3)
  (print 42)
  (print 1))
