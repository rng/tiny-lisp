import ctypes

libc = ctypes.CDLL('libc.so.6')
regmap = {0:0,    # eax
          1:3,    # ebx
          "rr":0, # eax
          "sp":4, # esp
          "fp":5, # ebp
          "gl":6, # esi
          "re":7, # edi
         }


PRIM1 = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_int)
PRIM3 = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_uint, ctypes.c_uint, ctypes.c_int)
def prim_print(v):
  print ">", v
  return 0


def prim_memcpy(dest, src, size):
  assert 0 < size <= 256
  #print "memcpy 0x%0x <- 0x%0x +%d" %(dest, src, size)
  libc.memcpy(dest, src, size)
  return dest
 

class Cg(object):
  def __init__(self):
    self.globs = libc.calloc(64, 4)
    # init code mem
    self.codememsize = libc.getpagesize()
    libc.mmap.restype = ctypes.c_uint
    self.codemem = libc.mmap(0, self.codememsize, 0x7, 0x22, -1, 0);
    self.heapmem = libc.mmap(0, self.codememsize, 0x7, 0x22, -1, 0);
    self.heapofs = 0
    self.reset()

  def reset(self):
    self.code = []
    self.t = -1
    # init global mem
    self._i8(0x55, 0x89, 0xe5)  # push %ebp; mov %esp, %ebp
    self._i8(0xb8+6) # mov $globs, %edx
    self._i32(self.globs)

  def __del__(self):
    libc.munmap(self.codemem, self.codememsize)
    libc.munmap(self.heapmem, self.codememsize)
    libc.free(self.globs)

  def prim_alloc(self, size):
    if (size%4) != 0:
      size += 4 - (size%4)
    #print "alloc 0x%x +%d" % (self.heapmem+self.heapofs, size)
    addr = self.heapmem+self.heapofs
    self.heapofs += size
    assert self.heapofs < self.codememsize
    return addr

  def _push(self):
    self.t+=1
    assert 2 > self.t >= 0
    return (self.t)

  def _pop(self, *regs):
    for r in regs:
      assert isinstance(r, (str, int))
      if r not in ["rr", "re", "sp", "fp", "gl"]:
        if self.t >= 0:
          self.t -= 1

  def unused(self, reg):
    self._pop(reg)

  def _i(self, ret, inst):
    self.code.append(inst)
    return ret

  def _i8(self, *v):
    for b in v: chr(b)
    self.code.extend(v)

  def _i32(self, v):
    self.code.append((v    ) & 0xff)
    self.code.append((v>>8 ) & 0xff)
    self.code.append((v>>16) & 0xff)
    self.code.append((v>>24) & 0xff)

  def _call(self, addr, nargs=1):
    self._i8(0xb8); self._i32(addr) # mov addr, %eax
    self._i8(0xff, 0xd0)  # call *%eax
    self._i8(0x83, 0xc4, 0x04*nargs) # add $(4*nargs), %esp

  def gen_int(self, v):
    rd = self._push()
    assert rd < 4
    if v == 0:
      self._i8(0x31, 0xc0|(regmap[rd]<<3)|(regmap[rd])) # xor %rd, %rd
    else:
      self._i8(0xb8+regmap[rd]) # mov v, %rd
      self._i32(v)
    return rd
  
  def gen_prim(self, fn, rs):
    self._pop(*rs)
    rd = self._push()
    assert rd == rs[0], "bad prim regs %s %s %s" % (fn, rd, rs)
    assert len(rs) == 1 or (rd != rs[1])
    if fn == "+":
      self._i8(0x01, 0xc0|(regmap[rs[1]]<<3)|(regmap[rd])) # add %rs[0], %rd
    elif fn == "-":
      self._i8(0x29, 0xc0|(regmap[rs[1]]<<3)|(regmap[rd])) # sub %rs[0], %rd
      self._i8(0xf7, 0xd8|regmap[rd])
    elif fn == "*":
      self._i8(0x0f, 0xaf, 0xc0|(regmap[rd]<<3)|(regmap[rs[1]])) # imul %rs[0], %rd
    elif fn == ">":
      self._i8(0x39, 0xc0|(regmap[rs[1]]<<3)|(regmap[rd])) # cmp %rs[0], %rd
      self._i8(0x0f, 0x9c, 0xc0|(regmap[rd])) # setl %rd
    elif fn == "<":
      self._i8(0x39, 0xc0|(regmap[rs[1]]<<3)|(regmap[rd])) # cmp %rs[0], %rd
      self._i8(0x0f, 0x9f, 0xc0|(regmap[rd])) # setg %rd
    elif fn == "=":
      self._i8(0x39, 0xc0|(regmap[rs[1]]<<3)|(regmap[rd])) # cmp %rs[0], %rd
      self._i8(0x0f, 0x94, 0xc0|(regmap[rd])) # sete %rd
    elif fn == "print":
      assert len(rs) == 1
      self.gen_push(rs[0])
      self._push()
      addr = ctypes.cast(PRIM1(prim_print), ctypes.c_void_p).value
      self._call(addr)
    else:
      print "'%s' %s %s" % (fn, rd, rs)
      raise
    return rd

  def gen_alloc(self, size):
    self.gen_push(self.gen_int(size))
    addr = ctypes.cast(PRIM1(self.prim_alloc), ctypes.c_void_p).value
    self._call(addr)
    rd = self._push()
    assert rd == 0
    return rd

  def gen_memcpy(self, rd, src, size):
    self.gen_push(self.gen_int(size))
    self.gen_push(self.gen_int(src))
    self.gen_push(rd)
    addr = ctypes.cast(PRIM3(prim_memcpy), ctypes.c_void_p).value
    self._call(addr, 3)

  def gen_push(self, rs):
    self._pop(rs)
    self._i8(0x50|regmap[rs])    # push %rs
    return rs

  def gen_pop(self, rd):
    self._i8(0x58|regmap[rd])    # pop %rd
    return rd

  def gen_cmp(self, rp):
    self._pop(rp)
    self._i8(0x85, 0xc0|(regmap[rp]<<3)|(regmap[rp])) # test %rp, %rp

  def gen_jmp(self, cond=False):
    pos = len(self.code)
    if cond:
      self._i8(0x0f, 0x84) # je
    else:
      self._i8(0xe9) # jmp
    self._i32(0)
    return pos

  def patch(self, jumpaddr):
    assert self.code[jumpaddr] in [0xe9, 0x0f]
    ofs = 2 if self.code[jumpaddr] == 0x0f else 1
    jb = jumpaddr + ofs
    v = len(self.code) - (jb + 4)
    self.code[jb+0] = (v    ) & 0xff
    self.code[jb+1] = (v>>8 ) & 0xff
    self.code[jb+2] = (v>>16) & 0xff
    self.code[jb+3] = (v>>24) & 0xff

  def gen_mov(self, rd, rs):
    self._pop(rs)
    if rs != rd:
      self._i8(0x89, 0xc0|(regmap[rs]<<3)|(regmap[rd])) # mov %rs, %rd
    return rd
  
  def gen_st(self, rs, rb, v):
    assert rb != "sp"
    if rb == "fp": v = -1-v
    self._i8(0x89, 0x40|(regmap[rs]<<3)|(regmap[rb]), v*4) # mov v(%rs), %rd
    return rs

  def gen_ld(self, rs, v, rd=None):
    assert rs != "sp"
    if rd is None: rd = self._push()
    if rs == "fp": v = -1-v
    self._i8(0x8b, 0x40|(regmap[rd]<<3)|(regmap[rs]), v*4) # mov v(%rs), %rd
    return rd

  def gen_ret(self, rs):
    self._pop(rs)
    self.gen_mov(0, rs)
    self._i8(0xc3) # ret

  def gen_chka(self, rs, n):
    self._pop(rs)
    self._i8(0x90) # nop FIXME
  
  def gen_chkt(self, rs, typ):
    self._i8(0x90) # nop FIXME

  def gen_call(self, rf, n, tail):
    self._pop(rf)
    if tail:
      self._i8(0x83, 0xc0, 0x03); # add $3, %eax
      self._i8(0xff, 0xe0) # jmp *%eax
    else:
      self._i8(0xff, 0xd0)  # call *%eax
    return "rr"

  def gen_halt(self):
    pass

  def gen_drop(self, n):
    self._i8(0x83, 0xc4, n*4) # add $(n*4), %esp

  def codeaddr(self, ofs):
    baseaddr = ctypes.cast(self.codemem, ctypes.c_void_p).value
    return baseaddr + ofs
  
  def funcaddr(self, addr):
    return self.codeaddr(addr) + 5 # base + addr + jump size

def disasm(cg):
  pass

def run(cg):
  code = bytearray(cg.code + 
                   [0x5d, 0xc3]) # pop %ebp; ret
  print "run (%d):" % len(code), "".join("%02x" % v for v in code)

  libc.memcpy(cg.codemem, str(code), len(code))
  func = ctypes.cast(cg.codemem, ctypes.CFUNCTYPE(ctypes.c_int))
  ret = func()
  return ret
