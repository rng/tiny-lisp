(def counter (lambda (start)
     (lambda (incr)
       (progn
         (set start (+ start incr))
         start))))

(def cnt (counter 2))
(def knt (counter 3))
(print (cnt 1))
(print (knt 1))
(print (cnt 2))
(print (knt 2))

(def fact (lambda (n)
  (if (> n 1)
    (* n (fact (- n 1)))
    1)))

(print (fact 5))

(def tailfact (lambda (n acc)
  (if (> n 1)
    (tailfact (- n 1) (* acc n))
    acc)))

(print (tailfact 20 1))

(def foo (lambda (a b)
  (progn
    (print a)
    (print b)
    )))

(foo 1 2)

